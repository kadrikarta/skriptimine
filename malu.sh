#!/bin/bash

echo "Millise kasutaja m2lukasutust soovid kuvada?"
read sisend

summa=0

ps="$(ps aux|grep $sisend|awk '{print $5 , $6}')"
#echo $ps

for i in $ps
do
	muutuja=$(echo $i | awk -F "," '{print $1}')
	summa=$(expr $summa + $muutuja)
done

echo "Summa on: $summa"

count=0 
while [ $(echo "$summa >= 1024" | bc) -eq 1 ]
do
	summa=$(echo "$summa / 1024" | bc -l)
	let "count++"
done

echo "Count: $count"
echo "Summa: $summa"

case $count in
0) yhik="K"
;;
1) yhik="M"
;;
2) yhik="G"
;;
3) yhik="T"
;;
esac

echo "Yhik: $yhik"

echo "Kasutaja $sisend m2lukasutus kokku: $summa $yhik."
exit 0
