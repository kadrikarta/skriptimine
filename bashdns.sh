#!/bin/bash

#K2esolev skript v6imaldab lisada ja muuta bind9 kirjeid.

#Skript eeldab, et: *bind0 on paigldatud ning esmased seaded on teostatud.
# 					*tsoonifailid asuvad kaustas /etc/bind/zones
#					*fail named.conf.local asub kaustas /etc/bind
#					*kodudomeen on in.dom. Sisestatakse ainult hostinimi.
#					*fail db.in.dom on olemas
#					*kasutatakse ainult C klassi IP aadresse (/24)

#Skripti v6ib k2ivitada kahte moodi:
#1)Andes skripti k2ivitades ette hostinime ja ip aadressi. 
#IP aadress anda seadsi, et punktide asemel on tyhikud. 
#St, et skripti v6ib k2ivitada n2iteks nii: ./bashdns.sh hostinimi 192 168 1 10
#
#2)Andes skripti k2ivitades ette faili. Sedasi: ./bashdns.sh /faili/asukoht
#Faili v6ib panna mitu kirjet. Iga kirje uuel real.
#Kirjed peavad olema failis kujul: hostinimi 192 168 1 10

#SKript t88tab kenasti aine iTee labori arvutis. Keskkond seatud 6ppej6u eeskujul.
#Erandiks: funktsioon lahendamine seisud 2 ja 3 ning funktsioon serialiuuendamine,
#need t88tavad mingitel kordadel ja teistel j2lle ei t88ta.
#
#Kui sisendiks on fail siis on v6imalik ainult lisada uut infot.
#Avastasin liiga hilja, et skript ei peatu, kui kasutajalt kysitakse, et lasta valida 
#olemasoleva info asendamine.

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#
#Pane t2hele, et m2rkisin sinu mugavuseks 2ra, kus skripti t88 algab!
#
#Leiad palju kommenteeritud echosid. Neid saab kasutada testimiseks =)

#Funktsioon kontrollib sisendite eksisteerimist systeemis 
function kontrollimine {
		
	#Massiiv k6ikidest bindi db.failidest
	dnsfailid=/etc/bind/zones/db.*
	
	for fail in $dnsfailid
	do
		#Konrollin sisendite olemasolu dns failides
		if [ "$fail" == "$tsoonitee/$revtsoon" ] #Sisestatud IP'le on olemas revtsoon
		then
			if [ "$fip" == "$5" ] #Sisestatud IP on olemas
			then
				if [ "$fiphost" == "$1.in.dom." ] #sisestatud hostinimi on olemas
				then
					local seis=1 #Sisestatud IP ja hostinimi on juba olemas
					#echo "seis on 1"
					return $seis
				else
					local seis=2 #Sisestatud IP on , hostinimi on teine
					#echo "seis on 2"
					return $seis
				fi
			else #IP ei ole leitud failis
				if [ "$fhost" == "$1.in.dom." ] #sisestatud hostinimi on failis
				then
					local seis=3 #sisestatud hostinimi on, kuid IP l6pp on teine
					#echo "seis on 3"
					return $seis
				else
					local seis=4 # puudub sisestatud IP ja host
					#echo "seis on 4"	
					return $seis
				fi
			fi	
		else #Sellist reverse tsooni ei ole	
			if [ "$fdomhost" == "$1" ] #db.in.dom sees on olemas sisestatud hostinimi
			then
				local seis=5 #IP pole kuid host on
				#echo "seis on 5"
				return $seis
			else
				local seis=6 #IP tervenisti ja hosti kumbagi pole
				#echo "seis on 6" 	
				return $seis
			fi	
		fi				
	done
}  

#Funktsiooni Serial v22rtuse uuendamiseks
function serialiuuendamine {
	
	if [ $1 -eq 1 ]
	then
		local fail=$(echo "$tsoonitee/$revtsoon")
	else
		local fail=$(echo "$tsoonitee/db.in.dom")
	fi	

	vanaserialigrep=$(echo $(grep -w Serial $fail 2>/dev/null))	
	#echo "Serialirida: $vanaserialigrep"
	vanaserial=$(echo "$vanaserialigrep" | awk '{print $1}')
	#echo "$vanaserial"
	((uusserial=$vanaserial+1))
	#echo "$uusserial"
	vanaserialirida=$(echo -e "$vanaserial\t; Serial")
	#echo "Vana: $vanaserialirida"
	uusserialirida=$(echo -e "$uusserial\t; Serial")
	#echo "Uus : $uusserialirida"
	sed -i -e "s/$vanaserialirida/$uusserialirida/g" $fail

}

#Funktsioon named.conf.local uuendamiseks
function namedconflocal {

	#FAILITEE PARANDADA
	echo -e "zone "$d.$c.$b.in-addr.arpa" {" >> /etc/bind/named.conf.local
	echo -e "\ttype master;" >> /etc/bind/named.conf.local
	echo -e "\tfile "$tsoonitee/db.$d.$c.$b";" >> /etc/bind/named.conf.local
	echo -e "};" >> /etc/bind/named.conf.local

}

#Funktsioon, mis teostab vajalikud muudatused vastavalt tuvastatud olukorrale
function lahendamine {

	case $1 in
	1)#Sisestatud info on juba systeemis
		echo "Leitud sisestatud hostile vastav ip aadress."
		echo "Pole midagi lisada ega muuta."	
	;;
	2)#Sisestatud IP on olemas, kuid host on teine
		local uusrida=$(echo -e "$e\tIN\tPTR\t$a.in.dom.")
		local vanarida=$(echo -e "$e\tIN\tPTR\t$fiphost")
		
		local uusrida1=$(echo -e "$a \tIN\tA\t$b.$c.$d.$e")
		local vanarida1=$(echo -e "$fdomip\tIN\tA\t$b.$c.$d.$e")
				
		echo "Sisestatud hostile vastab mingi teine IP" 
		echo "Vana info: $vanarida"
		echo "Uus  info: $uusrida"
		echo "Vana info: $vanarida1"
		echo "Uus  info: $uusrida1"
		echo "Kas soovid teha asenduse? Sisesta 1 v6i 0: 1= jah; 0= ei"
		read variant
		if [ $variant -eq 1 ]
		then
			echo "Asendan."
			sed -i -e "s/$vanarida/$uusrida/g" $tsoonitee/$revtsoon
			serialiuuendamine 1	
			
			sed -i -e "s/$vanarida1/$uusrida1/g" $tsoonitee/db.in.dom
			
			serialiuuendamine 2
		else
			echo "Ei muuda midagi."
		fi
	;;
	3)#db.ip-aadress fail on olemas, sisestatud hostinimi on, IP l6pp on teine.
		local vanaip=$((echo "$otsinhost") | awk '{print $1}')
		local uusrida=$(echo -e "$e \tIN\tPTR\t$fhost")
		local vanarida=$(echo -e "$vanaip\tIN\tPTR\t$fhost")
		
		local uusrida1=$(echo -e "$fhost\tIN\tA\t$b.$c.$d.$e")
		local vanarida1=$(echo -e "$fhost\tIN\tA\t$b.$c.$d.$vanaip")
		
		echo "Sisestatud hostinimele vastab teine IP aadress."
		echo "Vana info: $vanarida"
		echo "Uus  info: $uusrida"
		echo "Vana info: $vanarida1"
		echo "Uus  info: $uusrida1"
		echo "Kas soovid teha asenduse? Sisesta 1 v6i 0: 1= jah; 0= ei"
		read variant
		if [ $variant -eq 1 ]
		then
			echo "Asendan."
			sed -i -e "s/$vanarida/$uusrida/g" $tsoonitee/$revtsoon			
			serialiuuendamine 1	

			sed -i -e "s/$vanarida1/$uusrida1/g" $tsoonitee/db.in.dom
			serialiuuendamine 2
		else
			echo "Ei asenda."
		fi
	;;
	4)#db.ip-aadress fail on olemas. Puudub sisestatud host ja IP l6pp. 
		#Lisan faili db.ip-aadress sisestatud IP4 ja hosti
		echo "Lisan aadressi ja hostinime olemasolevatesse failidesse"
		echo -e "$e\tIN\tPTR\t$a.in.dom." >> $tsoonitee/$revtsoon
		serialiuuendamine 1
		echo -e "$a\tIN\tA\t$b.$c.$d.$e" >> $tsoonitee/db.in.dom
		serialiuuendamine 2
	;;
	5)#db.ip-aadress faili pole. IP pole kuid host on.
		echo "Sisestatud hostinimele vastab teine IP aadress."
		local uusip=$(echo "$b.$c.$d.$e")
		local uusrida=$(echo -e "$a\tIN\tA\t$uusip")
		local vanaip=$((echo "$otsindom") | awk '{print $4}')
		local vanarida=$(echo -e "$a\tIN\tA\t$vanaip")
		echo "Vana info: $vanarida"
		echo "Uus  info: $uusrida"
		echo "Kas soovid teha asenduse? Sisesta 1 v6i 0: 1= jah; 0= ei"
		read variant
		if [ $variant -eq 1 ]
		then
			echo "Asendan."
			cp /etc/bind/db.127 $tsoonitee/$revtsoon
			sed -i '$ d' $tsoonitee/$revtsoon
			sed -i '$ d' $tsoonitee/$revtsoon
			echo -e "$e\tIN\tPTR\t$a.in.dom." >> $tsoonitee/$revtsoon			
			serialiuuendamine 1
											
			sed -i -e "s/$vanarida/$uusrida/g" $tsoonitee/db.in.dom			
			serialiuuendamine 2
			namedconflocal
			
		else
			echo "Ei asenda."
		fi
	;;
	6)#Kogu sisestatud info on uus.
		echo "Loon uue tsooni."
		cp /etc/bind/db.127 $tsoonitee/$revtsoon
		sed -i '$ d' $tsoonitee/$revtsoon
		sed -i '$ d' $tsoonitee/$revtsoon
		echo -e "$e \tIN\tPTR\t$a.in.dom." >> $tsoonitee/$revtsoon
		serialiuuendamine 1
		echo -e "$a\tIN\tA\t$b.$c.$d.$e" >> $tsoonitee/db.in.dom
		serialiuuendamine 2
		namedconflocal
	;;	
	esac
}

#ip aadressi kontrollimise funktsioon
#Allikas: http://www.linuxjournal.com/content/validating-ip-address-bash-script
function ipkontroll {

local  ip=$1
    local  stat=1

    if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]
	then
        OIFS=$IFS
        IFS='.'
        ip=($ip)
        IFS=$OIFS
        [[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            && ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        stat=$?
    fi
    return $stat
}

#
#
####-----------------------------------####
###------------SKRIPTI ALGUS------------###
##---------------------------------------##										 	

#Kontrollin, et kasutaja on root
if [ $UID -ne 0 ]
then
	echo "Palun k2ivita skript uuesti juurkasutajana!"
	exit 1
fi

tsoonitee=$(echo "/etc/bind/zones")
#echo "$tsoonitee"


#Kontrollin skripti sisendeid ja k2ivitan funktsiooni kontrollimine nii mitu korda kui on vaja
if [ $# -eq 1 ]	#On yks sisend, fail 
then
	FAIL=$1	
			
	while IFS= read line	
	do
		a=$(echo "$line" | awk '{print $1}')
		b=$(echo "$line" | awk '{print $2}')
		c=$(echo "$line" | awk '{print $3}')
		d=$(echo "$line" | awk '{print $4}')
		e=$(echo "$line" | awk '{print $5}')
			
		ip=$(echo "$b.$c.$d.$e")
		#echo $ip
		
		ipkontroll $ip
		if [ $? -eq 0 ]
		then
			revtsoon=$(echo "db.$d.$c.$b")
			#echo "Tsoonifail on: $revtsoon"

			otsinip=$(echo $(grep -w $e $tsoonitee/$revtsoon 2>/dev/null))
			#echo "Otsitav ip j2rgi: $otsinip"
			fip=$((echo "$otsinip") | awk '{print $1}')
			#echo $fip
			fiphost=$((echo "$otsinip") | awk '{print $4}')

			otsinhost=$(echo $(grep -w $a $tsoonitee/$revtsoon 2>/dev/null))
			#echo "Otsitav hosti j2rgi: $otsinhost"
			fhost=$((echo "$otsinhost") | awk '{print $4}')
			#echo "hostinimi: $fhost"

			otsindom=$(echo $(grep -w $a $tsoonitee/db.in.dom 2>/dev/null)) 
			#echo "Otsitav hosti j2rgi indom failist: $otsindom"
			fdomhost=$((echo "$otsindom") | awk '{print $1}')
			
			otsindomip=$(echo $(grep -w $ip $tsoonitee/db.in.dom 2>/dev/null)) 
			fdomip=$((echo "$otsindomip") | awk '{print $1}')
		
			
			kontrollimine $a $b $c $d $e
			lahendamine $?
		else
			echo "Sisestatud ip aadress ei ole sobilik. J2tan sisendi vahele."			
		fi		
	done <"$FAIL"
else
	if [ $# -eq 5 ] #On viis sisendit, st info sisestati k2surealt
	then		
		a=$1
		b=$2
		c=$3
		d=$4
		e=$5
		
		ip=$(echo "$b.$c.$d.$e")
		#echo $ip		
		ipkontroll $ip
		
		if [ $? -eq 0 ]
		then		
			revtsoon=$(echo "db.$d.$c.$b")
			#echo "Tsoonifail on: $revtsoon"

			otsinip=$(echo $(grep -w $e $tsoonitee/$revtsoon 2>/dev/null))
			#echo "Otsitav ip j2rgi: $otsinip"
			fip=$((echo "$otsinip") | awk '{print $1}')
			#echo $fip
			fiphost=$((echo "$otsinip") | awk '{print $4}')

			otsinhost=$(echo $(grep -w $a $tsoonitee/$revtsoon 2>/dev/null))
			#echo "Otsitav hosti j2rgi: $otsinhost"
			fhost=$((echo "$otsinhost") | awk '{print $4}')
			#echo "hostinimi: $fhost"

			otsindom=$(echo $(grep -w $a $tsoonitee/db.in.dom 2>/dev/null)) 
			#echo "Otsitav hosti j2rgi indom failist: $otsindom"
			fdomhost=$((echo "$otsindom") | awk '{print $1}')
			
			otsindomip=$(echo $(grep -w $ip $tsoonitee/db.in.dom 2>/dev/null)) 
			fdomip=$((echo "$otsindomip") | awk '{print $1}')

			kontrollimine $a $b $c $d $e
			lahendamine $?
		else
			echo "Sisestatud ip aadress ei ole sobilik. L6petan."
			exit 1
		fi
	else
		echo "Anna sisendiks failiteekond v6i hostinimi ja IP aadress n6utud kujul (loe juhendit skripri algusest)"
		exit 1
	fi
fi

service bind9 restart
echo "Skript l6petab!"
exit 0