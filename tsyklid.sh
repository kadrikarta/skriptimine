#!/bin/bash

COUNTER=20
until [ $COUNTER -lt 10 ]; do
	echo COUNTER $COUNTER
	let COUNTER-=1
done

exit 0 

COUNTER=0
while [ $COUNTER -lt 10 ]; do
	echo The counter is $COUNTER
	let COUNTER=COUNTER+1
done

exit 1

FILES="$@"
for f in $FILES
do
	#if .bak backup file exists, read next file
	if [ -f ${f}.bak ]
	then
		echo "Skiping $f file..."
		continue #read next file and skip the cp command
	fi
		# We are here measn no backup file exists, just use co command to copy file
/bin/cp $f $f.bak
done	

exit 1

for file in /etc/*
do
	if [ "${file}" == "/etc/resolv.conf" ]
	then
	countNameservers=$(grep -c nameserver /etc/resolv.conf)
	echo "Total ${countNameservers} nameservers defined in ${file}"
	break
	fi
done

exit 1

for (( ; ; ))
do 
echo "infinite loops [ hit CTRL+C to stop]"
done

exit 1

for (( c=1; c<=5; c++ ))
do
echo "Welcome $c times"
done

exit 1

echo "Bash version  ${BASH_VERSION}..."
for i in {0..10..2}
do
echo "Welcome $i times"
done

exit 1

for i in {1..5}
do
echo "Welcome $i times"
done

exit 1


for i in 1 2 3 4 5 
do
echo "Welcome $i times"
done
