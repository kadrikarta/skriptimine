#!/bin/bash

#Seadistab keskkonna
export LC_ALL=C

#Kas on k2ivitatud juurena?
if [ $UID -ne 0 ]
then
	echo "K2ivita skript juurkasutaja 6igustes!"
	exit 1
fi

#Kas on antud ette oige arv muutujaid?
if [[ $# -ne 2 && $# -ne 3 ]]
then
	echo "Vale arv muutujaid! Anna sisendiks v2hemalt kasut ja grupp."
	exit 1
fi

if [ $# -eq 2 ]
then
	share=$2
else
	share=$3
	
fi	

#Kontrollib, kas samba on paigaldatud ja paigaldab kui vaja
type smbd > /dev/null 2>&1
if [ $? -ne 0 ]
then
	echo "Paigaldan samba"
	(apt-get update > /dev/null 2>&1 && apt-get install samba -y) || exit 1
	# Kui paigaldamine ei onnestu siis v2lju
fi
echo "Samba olemas"

#Kontrollib, kas kaust on olemas ja loob kui vaja
test -d $1 || mkdir -p $1
echo "Kaust olemas"

#Kontrollib kas grupp on olemas ja loob kui vaja
getent group $2 > /dev/null || addgroup $2 > /dev/null
echo "Grupp olemas"

#Koopia samba confist
sudo cp /etc/samba/smb.conf /etc/samba/smb.conf.old

if [ $? -eq 0 ]
then 
	echo "Samba konfi koopia tehtud"
else
	echo "Samba konfi kopeerimine eba6nnestus"
	exit 1
fi

#Samba confi muutmine
echo -e "\n[$share]\ncomment=Praktikumi kaust\npath=$1\nwritable=yes\nvalid users=@$2\nforce group=$2\nbrowsable=yes\ncreate mask=0655\ndirectory mask=0775" >> /etc/samba/smb.conf

if [ $? -eq 0 ]
then
	echo "Samba konfi muutmine oli edukas!"
else
	echo "Konfi muutmine ei 6nnestunud!"
	exit 1
fi


#Samba reload
 /etc/init.d/samba reload
if [ $? -eq 0 ]
then 
	echo "Samba reload tehtud"
	echo "Valmis!"
else
	echo "Samba reload ei 6nnestunud!"
	exit 1
fi
